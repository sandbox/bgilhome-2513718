<?php

/**
 * Implements hook_default_message_type_category().
 */
function message_system_default_message_type_category() {
  $items = array();
  $items['system_message'] = entity_create('message_type_category', array(
    'description' => 'A message produced by the system (eg. account actions)',
  ));
  return $items;
}

/**
 * Implements hook_default_message_type().
 */
function message_system_default_message_type() {
  $items = array();

  $items['user_register_admin_created'] = entity_create('message_type', array(
    'description' => 'System: User created by admin',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'An administrator created an account for you at [site:name]'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>A site administrator at [site:name] has created an account for you. You may now log in by clicking this link or copying and pasting it to your browser:</p>

<p>[message:user:one-time-login-url]</p>

<p>This link can only be used once to log in and will lead you to a page where you can set your password.</p>

<p>After setting your password, you will be able to log in at [site:login-url] in the future using:</p>

<p>username: [message:user:name]<br />
password: Your password</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_register_pending_approval'] = entity_create('message_type', array(
    'description' => 'System: User created pending approval',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account details for [message:user:name] at [site:name] (pending admin approval)'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>Thank you for registering at [site:name]. Your application for an account is currently pending approval. Once it has been approved, you will receive another e-mail containing information about how to log in, set your password, and other details.</p>

<p><br />
-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_register_no_approval_required'] = entity_create('message_type', array(
    'description' => 'System: User created',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account details for [message:user:name] at [site:name]'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>Thank you for registering at [site:name]. You may now log in by clicking this link or copying and pasting it to your browser:</p>

<p>[message:user:one-time-login-url]</p>

<p>This link can only be used once to log in and will lead you to a page where you can set your password.</p>

<p>After setting your password, you will be able to log in at [site:login-url] in the future using:</p>

<p>username: [message:user:name]<br />
password: Your password</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_status_activated'] = entity_create('message_type', array(
    'description' => 'System: User activated',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account details for [message:user:name] at [site:name] (approved)'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>Your account at [site:name] has been activated.</p>

<p>You may now log in by clicking this link or copying and pasting it into your browser:</p>

<p>[message:user:one-time-login-url]</p>

<p>This link can only be used once to log in and will lead you to a page where you can set your password.</p>

<p>After setting your password, you will be able to log in at [site:login-url] in the future using:</p>

<p>username: [message:user:name]<br />
password: Your password</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_status_blocked'] = entity_create('message_type', array(
    'description' => 'System: User blocked',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account details for [message:user:name] at [site:name] (blocked)'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>Your account on [site:name] has been blocked.</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_cancel_confirm'] = entity_create('message_type', array(
    'description' => 'System: User cancel confirmation',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account cancellation request for [message:user:name] at [site:name]'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>A request to cancel your account has been made at [site:name].</p>

<p>You may now cancel your account on [site:url-brief] by clicking this link or copying and pasting it into your browser:</p>

<p>[message:user:cancel-url]</p>

<p>NOTE: The cancellation of your account is not reversible.</p>

<p>This link expires in one day and nothing will happen if it is not used.</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_status_canceled'] = entity_create('message_type', array(
    'description' => 'System: User canceled',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Account details for [message:user:name] at [site:name] (cancelled)'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>Your account on [site:name] has been canceled.</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));
  $items['user_password_reset'] = entity_create('message_type', array(
    'description' => 'System: User password reset',
    'message_category' => 'system_message',
    'message_text' => array(
      LANGUAGE_NONE => array(
        array(
          'format' => 'plain_text',
          'value' => 'Replacement login information for [message:user:name] at [site:name]'),
        array(
          'format' => 'filtered_html',
          'value' => '<p>[message:user:name],</p>

<p>A request to reset the password for your account has been made at [site:name].</p>

<p>You may now log in by clicking this link or copying and pasting it to your browser:</p>

<p>[message:user:one-time-login-url]</p>

<p>This link can only be used once to log in and will lead you to a page where you can set your password. It expires after one day and nothing will happen if it\'s not used.</p>

<p>-- &nbsp;[site:name] team</p>'),
      ),
    ),
  ));

  return $items;
}

/**
 * Implements hook_default_message_type_alter().
 * Use the LANGUAGE_NONE values for the other languages in multilingual sites.
 */
function message_system_default_message_type_alter(&$items) {
  if (module_exists('locale')) {
    $languages = locale_language_list();
    foreach ($languages as $langcode => $langname) {
      foreach ($items as $message_type => $item) {
        if ($item->module != 'message_system') {
          continue;
        }
        if (isset($items[$message_type]->message_text[LANGUAGE_NONE])) {
          $items[$message_type]->message_text[$langcode] = $items[$message_type]->message_text[LANGUAGE_NONE];
        }
      }
    }
  }
}

/**
 * Refresh the fields attached to the message types we support.
 */
 // Add a field to system message bundles
// function message_system_message_field_refresh() {
//   $fields['message_account']['field'] = array (
//     'type' => 'entityreference',
//     'module' => 'entityreference',
//     'cardinality' => '1',
//     'translatable' => FALSE,
//     'settings' => array(
//       'target_type' => 'user',
//       'handler' => 'base',
//       'handler_settings' => array(
//         'target_bundles' => array(),
//         'sort' => array(
//           'type' => 'property',
//           'property' => 'order_id',
//           'direction' => 'ASC',
//         ),
//       ),
//     ),
//     'locked' => TRUE,
//   );
//   $bundles = array(
//     'user_created_admin',
//     'user_created_pending',
//     'user_created',
//     'user_activated',
//     'user_blocked',
//     'user_cancel_confirmation',
//     'user_cancelled',
//     'user_password_recovery',
//   );
//   $fields['message_account']['instances'][] = array(
//     'entity_type' => 'message',
//     'bundles' => $bundles,
//     'label' => 'User',
//     'required' => TRUE,
//     'widget' => array(
//       'type' => 'entityreference_autocomplete',
//       'module' => 'entityreference',
//       'settings' => array(
//         'match_operator' => 'CONTAINS',
//         'size' => '60',
//         'path' => '',
//       ),
//     ),
//     'settings' => array(),
//     'display' => array(
//       'default' => array(
//         'label' => 'above',
//         'type' => 'entityreference_label',
//         'settings' => array(
//           'link' => FALSE,
//         ),
//         'module' => 'entityreference',
//         'weight' => 0,
//       ),
//     ),
//   );
//
//   drupal_alter('message_system_message_fields', $fields);
//
//   // Create the missing fields.
//   foreach ($fields as $field_name => $info) {
//     $field = $info['field'];
//     $field += array(
//       'field_name' => $field_name,
//     );
//     if (!field_info_field($field_name)) {
//       field_create_field($field);
//     }
//
//     foreach ($info['instances'] as $instance) {
//       foreach ($instance['bundles'] as $bundle) {
//         $instance['bundle'] = $bundle;
//         unset($instance['bundles']);
//         $instance['field_name'] = $field_name;
//         if (!field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
//           field_create_instance($instance);
//         }
//       }
//     }
//   }
// }
//
// /**
//  * Implements hook_field_access().
//  */
// function message_system_field_access($op, $field, $entity_type, $entity, $account) {
//   // Deny editability of this field, it's provided by Rules
//   if ($op == 'edit' && $field['field_name'] == 'message_account') {
//     return FALSE;
//   }
// }
