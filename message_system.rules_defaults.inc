<?php

// EDIT: now using hook_mail_alter instead of Rules to intercept system (user) mails

// /**
//  * Implements hook_default_rules_configuration().
//  */
// function message_system_default_rules_configuration() {
//   $rules['rules_user_created_admin'] = entity_import('rules_config', '{ "rules_user_created_admin" : {
//     "LABEL" : "System: User created by admin",
//     "PLUGIN" : "reaction rule",
//     "OWNER" : "rules",
//     "REQUIRES" : [ "rules" ],
//     "ON" : { "user_insert" : [] },
//     "IF" : [
//       { "user_has_role" : {
//           "account" : [ "site:current-user" ],
//           "roles" : { "value" : { "3" : "3" } }
//         }
//       },
//       { "NOT user_is_blocked" : { "account" : [ "account" ] } }
//     ],
//     "DO" : [
//       { "entity_create" : {
//           "USING" : {
//             "type" : "message",
//             "param_type" : "user_created_admin",
//             "param_user" : [ "account" ]
//           },
//           "PROVIDE" : { "entity_created" : { "message" : "Message" } }
//         }
//       },
//       { "entity_save" : { "data" : [ "message" ], "immediate" : 1 } },
//       { "message_notify_process" : {
//           "message" : [ "message" ],
//           "save_on_fail" : 0,
//           "save_on_success" : 0
//         }
//       }
//     ]
//   }
//   }');
//
//   $rules['rules_user_created_pending'] = entity_import('rules_config', '{ "rules_user_created_pending" : {
//     "LABEL" : "System: User created pending approval",
//     "PLUGIN" : "reaction rule",
//     "OWNER" : "rules",
//     "REQUIRES" : [ "rules" ],
//     "ON" : { "user_insert" : [] },
//     "IF" : [
//       { "user_is_blocked" : { "account" : [ "account" ] } }
//     ],
//     "DO" : [
//       { "entity_create" : {
//           "USING" : {
//             "type" : "message",
//             "param_type" : "user_created_pending",
//             "param_user" : [ "account" ]
//           },
//           "PROVIDE" : { "entity_created" : { "message" : "Message" } }
//         }
//       },
//       { "entity_save" : { "data" : [ "message" ], "immediate" : 1 } },
//       { "message_notify_process" : {
//           "message" : [ "message" ],
//           "save_on_fail" : 0,
//           "save_on_success" : 0
//         }
//       }
//     ]
//   }
//   }');
//
//   $rules['rules_user_created'] = entity_import('rules_config', '{ "rules_user_created" : {
//     "LABEL" : "System: User created",
//     "PLUGIN" : "reaction rule",
//     "OWNER" : "rules",
//     "REQUIRES" : [ "rules" ],
//     "ON" : { "user_insert" : [] },
//     "IF" : [
//       { "NOT user_has_role" : {
//           "account" : [ "site:current-user" ],
//           "roles" : { "value" : { "3" : "3" } }
//         }
//       },
//       { "NOT user_is_blocked" : { "account" : [ "account" ] } }
//     ],
//     "DO" : [
//       { "entity_create" : {
//           "USING" : {
//             "type" : "message",
//             "param_type" : "user_created",
//             "param_user" : [ "account" ]
//           },
//           "PROVIDE" : { "entity_created" : { "message" : "Message" } }
//         }
//       },
//       { "entity_save" : { "data" : [ "message" ], "immediate" : 1 } },
//       { "message_notify_process" : {
//           "message" : [ "message" ],
//           "save_on_fail" : 0,
//           "save_on_success" : 0
//         }
//       }
//     ]
//   }
//   }');
//
//   return $rules;
// }
